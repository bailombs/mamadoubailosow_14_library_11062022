import { Modal, useModal } from 'modal-react-sow'
import React from 'react'


const App = () => {
  const { isOpen, toggle } = useModal()
  return (
    <section className='main'>
      <h1>Contenu principal <br /> Peut etre un formulaire ou du texte</h1>
      <button className='open' onClick={() => { toggle() }}>Ouvrir Modal</button>

      <Modal
        isOpen={isOpen}
        toggle={toggle}
        modalMessage='Employee Created'
      />
    </section>

  )
}

export default App;
