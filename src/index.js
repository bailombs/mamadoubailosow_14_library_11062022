import PropTypes from 'prop-types';
import { default as React } from 'react';
import { useModal } from './useOpen';
export { useModal };

/**
 *  @description Component
 *
 * @param   {object}      props
 * @param   {string}      props.isOpen       
 * @param   {function}    props.toggle    
 * @param   {string}      props.modalMessage   
 * @param   {string}      props.modalBgColor 
 * @param   {string}      props.iconClassName           
 * @param   {string}      props.modalContentBgColor            
 * @param   {string}      props.color           
 * 
 * @returns {Reactnode}   
 */
export const Modal = ({ isOpen, toggle, modalMessage, modalBgColor, iconClassName, modalContentBgColor, color }) => {

  const modalStyle = {
    position: 'absolute',
    top: '0',
    left: '0',
    width: '100%',
    height: '100%',
    backgroundColor: modalBgColor ? modalBgColor : 'rgba(0, 0, 0, 0.65)'
  }
  const modalContentStyle = {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    position: 'relative',
    width: '50%',
    backgroundColor: modalContentBgColor ? modalContentBgColor : 'seagreen',
    color: color ? color : '#ddd',
    padding: '2rem',
    borderRadius: '5px',
    boxShadow: 'rgba(0, 0, 0, 0.35) 0px 5px 15px',
    height: '2em',
    margin: '25rem auto',
    boxSizing: 'border-box',
    animation: 'display 300ms ease-out'
  }
  '\
  @keyframes display {\
    from {\
      transform: scale(0.9);\
    }\ to {\
      transform: scale(1);\
    }\
  }'

  const wrapperButtonClose = {
    position: 'absolute',
    right: '-15px',
    top: '-15px'
  }

  const buttonClose = {
    border: '0',
    backgroundColor: '#ccc',
    color: '#191B26',
    width: '30px',
    height: '30px',
    lineHeight: '30px',
    fontSize: '1.5rem',
    fontWeight: 'bold',
    textAlign: 'center',
    cursor: 'pointer',
    borderRadius: '50%'
  }


  return (
    <React.Fragment>
      {
        isOpen ?
          (<section
            style={modalStyle}
          >
            <div style={modalContentStyle}>
              <div style={wrapperButtonClose}>
                <button
                  className={iconClassName}
                  onClick={() => toggle()}
                  style={buttonClose}
                >
                  x
                </button>
              </div>
              <h3 style={{
                textAlign: 'center',
                margin: '0',
                padding: '0'
              }}>
                {modalMessage}
              </h3>
            </div>
          </section>) : null
      }
    </React.Fragment>
  );
};



Modal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  modalMessage: PropTypes.string.isRequired,
  modalBgColor: PropTypes.string,
  modalContentBgColor: PropTypes.string,
  iconClassName: PropTypes.string,
  color: PropTypes.string
}