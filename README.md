# modal-react-sow

> Made with create-react-library

[![NPM](https://img.shields.io/npm/v/modal-react-sow.svg)](https://www.npmjs.com/package/modal-react-sow) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save modal-react-sow or yarn add modal-react-sow
```

## Usage

```jsx
import React from 'react'
//import Modal and useModal ffrom modal-react-sow
import { Modal, useModal } from 'modal-react-sow'

const App = () => {
  // use Modal hook to use
  const { isOpen, toggle } = useModal()

  return (
    <section className='main'>
      <h1>
        {' '}
        Your Main content <br /> May be a form or text content
      </h1>
      {/*your own button, you just add the toggle function in your button*/}
      <button
        className='open'
        onClick={() => {
          toggle()
        }}
      >
        Ouvrir Modal
      </button>
      <Modal isOpen={isOpen} toggle={toggle} modalMessage='Employee Created' />
    </section>
  )
}

export default App
```

### Custom modal in the component Modal

1 - custom modal message by the prop modalMessage

2 - custom modal Background color by the prop modalBgColor

3 - custom modal icon by the prop iconClassName

4 - custom modal content Background color by the prop modalContentBgColor'

5 - custom modal color text the prop color '

## License

MIT © [Bailo sow ]
